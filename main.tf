module s3_app {
  source = "./modules/s3"

  environment = var.environment

}

module beanstalk {
  source = "./modules/beanstalk"

  bice_vpc_id              = var.bice_vpc_id
  bice_name_application    = var.bice_name_application
  bice_environment         = var.bice_environment
  bice_solution_stack_name = var.bice_solution_stack_name
  bucket_id                = module.s3_app.s3_id
  object_id                = module.s3_app.s3_app_source_id
  environment              = var.environment

  #depends_on              = [module.s3_app]

}
