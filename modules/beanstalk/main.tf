resource aws_elastic_beanstalk_application application {
  name        = format("%s-%s", var.bice_name_application, lower(var.environment))
  description = var.description


}

resource aws_elastic_beanstalk_application_version default {
  name        = format("%s-version-%s", var.bice_name_application, lower(var.environment))
  application = aws_elastic_beanstalk_application.application.name
  description = "application version created by terraform"
  bucket      = var.bucket_id
  key         = var.object_id

  depends_on = [aws_elastic_beanstalk_application.application]
}

resource aws_elastic_beanstalk_environment environment {
  name                = format("%s-%s", var.bice_environment, lower(var.environment))
  application         = aws_elastic_beanstalk_application.application.name
  solution_stack_name = var.bice_solution_stack_name

  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = var.bice_vpc_id
    resource  = ""
  }

  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "subnet-0567139beafa866fa"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "aws-elasticbeanstalk-ec2-role"
  }

  depends_on = [aws_elastic_beanstalk_application.application, aws_elastic_beanstalk_application_version.default]
}