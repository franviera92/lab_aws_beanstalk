variable "aws_region" {
  type        = string
  default     = "us-east-1"
  description = "Reion AWS"
}
variable "bice_vpc_id" {
  type        = string
  default     = ""
  description = "VPC ID"
}
variable "bice_name_application" {
  type        = string
  default     = ""
  description = "Name application"
}
variable "bice_environment" {
  type        = string
  default     = ""
  description = "Name environment"
}
variable "bice_solution_stack_name" {
  type        = string
  default     = ""
  description = "solution_stack_name"
}

variable "bucket_id" {
  type        = string
  default     = ""
  description = "Bucket ID"
}
variable "object_id" {
  type        = string
  default     = ""
  description = "Object ID"
}

variable "environment" {
  type        = string
  default     = "dev"
  description = "Environment, e.g. 'prod', 'staging', 'dev', 'pre-prod', 'UAT'"
}

variable "description" {
  type        = string
  default     = "Example Beanstalk Bicevida"
  description = "Elastic Beanstalk Application description"
}
