## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 3.14.1 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 3.14.1 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| aws\_region | Reion AWS | `string` | `"us-east-1"` | no |
| bice\_environment | Name environment | `string` | `""` | no |
| bice\_name\_application | Name application | `string` | `""` | no |
| bice\_solution\_stack\_name | solution\_stack\_name | `string` | `""` | no |
| bice\_vpc\_id | VPC ID | `string` | `""` | no |
| bucket\_id | Bucket ID | `string` | `""` | no |
| description | Elastic Beanstalk Application description | `string` | `"Example Beanstalk Bicevida"` | no |
| environment | Environment, e.g. 'prod', 'staging', 'dev', 'pre-prod', 'UAT' | `string` | `"dev"` | no |
| object\_id | Object ID | `string` | `""` | no |

## Outputs

No output.

