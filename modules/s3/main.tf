module s3_bucket {
  source        = "terraform-aws-modules/s3-bucket/aws"
  version       = "1.16.0"
  bucket        = format("%s-bice-apps", lower(var.environment))
  acl           = "private"
  force_destroy = true
  versioning = {
    enabled = false
  }
}

resource aws_s3_bucket_object default {
  bucket = module.s3_bucket.this_s3_bucket_id
  key    = "app.zip"
  source = "${path.module}/app.zip"

  depends_on = [module.s3_bucket]
}