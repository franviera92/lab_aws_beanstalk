variable "environment" {
  type        = string
  default     = "dev"
  description = "Environment, e.g. 'prod', 'staging', 'dev', 'pre-prod', 'UAT'"
}
variable "aws_region" {
  type        = string
  default     = "us-east-1"
  description = "Reion AWS"
}
