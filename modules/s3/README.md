## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 3.14.1 |

## Providers

| Name | Version |
|------|---------|
| aws | ~> 3.14.1 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| aws\_region | Reion AWS | `string` | `"us-east-1"` | no |
| environment | Environment, e.g. 'prod', 'staging', 'dev', 'pre-prod', 'UAT' | `string` | `"dev"` | no |

## Outputs

| Name | Description |
|------|-------------|
| s3\_app\_source\_id | S3 name |
| s3\_id | S3 ID |

