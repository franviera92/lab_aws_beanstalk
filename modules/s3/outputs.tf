
output "s3_id" {
  description = "S3 ID"
  value       = module.s3_bucket.this_s3_bucket_id
}
output "s3_app_source_id" {
  description = "S3 name"
  value       = aws_s3_bucket_object.default.id
}