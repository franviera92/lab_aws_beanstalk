JOB_ENV?=dev
ifdef CI_COMMIT_TAG
	JOB_ENV:=prd
else ifeq ($(CI_COMMIT_REF_NAME), master)
	JOB_ENV:=qa
endif

export JOB_ENV

.PHONY: apply
apply: init
	@./ci/terraform.sh terraform_apply

.PHONY: destroy
destroy: plan-destroy
	@./ci/terraform.sh terraform_destroy

.PHONY: init
init:
	@./ci/terraform.sh terraform_init

.PHONY: plan
plan: init
	@./ci/terraform.sh terraform_plan

.PHONY: plan-destroy
plan-destroy: init
	@./ci/terraform.sh terraform_plan_destroy

.PHONY: refresh
refresh: init
	@./ci/terraform.sh terraform_refresh

.PHONY: validate
validate: init
	@./ci/terraform.sh terraform_validate
