## Tabla de contenidos
- [Tabla de contenidos](#tabla-de-contenidos)
- [Deploy APPS Beanstalk](#deploy-apps-beanstalk)
- [Config variables environment GITLAB CI](#config-variables-environment-gitlab-ci)
  - [Backend](#backend)
  - [Beanstalk](#beanstalk)
  - [Flow CI:](#flow-ci)
    - [- Validate:](#ullivalidateliul)
    - [- Plan:](#ulliplanliul)
    - [- Apply:](#ulliapplyliul)
    - [- Destroy:](#ullidestroyliul)
- [Requirements](#requirements)
- [Providers](#providers)
- [Inputs](#inputs)
- [Outputs](#outputs)
  - [Componentes Terraform:](#componentes-terraform)
    - [Módulos Terraform:](#módulos-terraform)
- [Example main.tf](#example-maintf)
  - [Gestion de ambientes](#gestion-de-ambientes)
    - [Ejemplo de configuración de variables de GITLAB CI por entorno](#ejemplo-de-configuración-de-variables-de-gitlab-ci-por-entorno)

## Deploy APPS Beanstalk

El presente proyecto tiene como finalidad desplegar un app sobre Beanstalk

## Config variables environment GITLAB CI

### Backend
 - TF_BACKEND_BUCKET=name_bucket
 - TF_BACKEND_KEY=key-to-tfstate

### Beanstalk
 -  bice\_environment
 -  bice\_name\_application
 -  bice\_solution\_stack\_name
 -  bice\_vpc\_id
 -  environment

### Flow CI:

![FLow CI](images/flow_ci.png)

#### - Validate:

- Etapa donde se validan y descargan archivos de terraform.

#### - Plan:

- Etapa en la que se realiza la planificación de los recursos a crear.

#### - Apply:

- Etapa en la que crean los recursos de s3 y beanstalk

#### - Destroy:

- Etapa en la que se destruyen todos los recursos creados de s3 y beanstalk

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | ~> 3.14.1 |

## Providers

No provider.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| bice\_environment | Name environment | `string` | `"bicebeanstalk"` | yes |
| bice\_name\_application | Name application | `string` | `"bicebeanstalk"` | yes |
| bice\_solution\_stack\_name | solution\_stack\_name | `string` | `"64bit Amazon Linux 2 v3.1.3 running Python 3.7"` | yes |
| bice\_vpc\_id | VPC ID | `string` | `"vpc-0871c101bbdaf426e"` | yes |
| environment | Environment, e.g. 'prod', 'staging', 'dev', 'pre-prod', 'UAT' | `string` | `"dev"` | yes |

## Outputs

No output.

### Componentes Terraform:

####  Módulos Terraform:

- S3
    - [Readme Module S3](https://gitlab.com/franviera92/lab_aws_beanstalk/-/tree/master/modules/s3)
- Beanstalk
    - [Readme Module Beanstalk](https://gitlab.com/franviera92/lab_aws_beanstalk/-/tree/master/modules/beanstalk)

## Example main.tf

```hcl
module s3_app {
  source = "./modules/s3"

  environment = var.environment

}

module beanstalk {
  source = "./modules/beanstalk"

  bice_vpc_id              = var.bice_vpc_id
  bice_name_application    = var.bice_name_application
  bice_environment         = var.bice_environment
  bice_solution_stack_name = var.bice_solution_stack_name
  bucket_id                = module.s3_app.s3_id
  object_id                = module.s3_app.s3_app_source_id
  environment              = var.environment

  #depends_on              = [module.s3_app]

}

```

### Gestion de ambientes
La gestión del entorno se configura según la definición model branch.

Es decir:
  - Todas las etiquetas generadas definen el entorno productivo.
  - La rama Master define el entorno para qa.
  - Todas las ramas excepto master se consideran entornos de desarrollo


#### Ejemplo de configuración de variables de GITLAB CI por entorno

- Tags = Production

  La configuracion de variables para el entorno productivo de seguir la siguiente nomenclatura con el prefijo prod_

  prod\_bice_vpc_id "vpc-id"


- La rama Master = Qa

  La configuracion de variables para el entorno qa debe seguir la siguiente nomenclatura con el prefijo qa_

  qa\_bice_vpc_id "vpc-id"

- Todas las ramas excepto Master = dev

 La configuracion de variables para los entornos de desasarrollos deben seguir la siguiente nomenclatura segun su nombre de branch dev_ 

  dev\_bice_vpc_id "vpc-id"