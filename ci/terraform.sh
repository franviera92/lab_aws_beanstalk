#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

CONVERT_REPORT='([.resource_changes[]?.change.actions?]|flatten)|{"create":(map(select(.=="create"))|length),"update":(map(select(.=="update"))|length),"delete":(map(select(.=="delete"))|length)}'
PLAN=plan.tfplan
PLAN_JSON=${PLAN_JSON:-tfplan.json}
TF_PARALLELISM=${TF_PARALLELISM:-3}
TF_BIN=terraform

echo "You will work with: "
echo ${JOB_ENV};

export TF_VAR_environment=${JOB_ENV}
export TF_VAR_bice_vpc_id=$(eval echo "\$${JOB_ENV}_bice_vpc_id");
export TF_VAR_bice_name_application=$(eval echo "\$${JOB_ENV}_bice_name_application");
export TF_VAR_bice_environment=$(eval echo "\$${JOB_ENV}_bice_environment");
export TF_VAR_bice_solution_stack_name=$(eval echo "\$${JOB_ENV}_bice_solution_stack_name");


echo "Variables configured"

function terraform_destroy {
    echo "Hi, I need variables based on ${JOB_ENV}"
    $TF_BIN workspace show
    $TF_BIN destroy -auto-approve -parallelism=$TF_PARALLELISM
}

function terraform_plan_destroy {
    echo "Hi, I need variables based on ${JOB_ENV}"
    $TF_BIN plan -destroy
}

function terraform_apply {
    echo "Hi, I need variables based on ${JOB_ENV}"
    $TF_BIN workspace show
    $TF_BIN apply -auto-approve -parallelism=$TF_PARALLELISM
}

function terraform_plan {
    echo "Hi, I need variables based on ${JOB_ENV}"
    $TF_BIN plan -out=$PLAN
    $TF_BIN show --json $PLAN|jq -r $CONVERT_REPORT > $PLAN_JSON
}

function terraform_validate {
    $TF_BIN validate
}

function terraform_init () {
    JOB_ENV=$(echo "$JOB_ENV" | tr '[:upper:]' '[:lower:]')
    $TF_BIN init -backend-config="bucket=${TF_BACKEND_BUCKET}" \
                   -backend-config="key=${TF_BACKEND_KEY}" \
                   -backend-config="region=us-east-1" \
                   -get-plugins=true

    $TF_BIN workspace select $JOB_ENV || $TF_BIN workspace new $JOB_ENV
}

function terraform_refresh () {
    $TF_BIN refresh
}

$1
