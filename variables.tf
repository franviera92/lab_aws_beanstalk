variable "environment" {
  type        = string
  default     = "dev"
  description = "Environment, e.g. 'prod', 'staging', 'dev', 'pre-prod', 'UAT'"
}
variable "bice_vpc_id" {
  type        = string
  default     = "vpc-0871c101bbdaf426e"
  description = "VPC ID"
}
variable "bice_name_application" {
  type        = string
  default     = "bicebeanstalk"
  description = "Name application"
}
variable "bice_environment" {
  type        = string
  default     = "bicebeanstalk"
  description = "Name environment"
}
variable "bice_solution_stack_name" {
  type        = string
  default     = "64bit Amazon Linux 2 v3.1.3 running Python 3.7"
  description = "solution_stack_name"
}
